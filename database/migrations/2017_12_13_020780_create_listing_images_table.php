<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 100);
            $table->text('url');
            $table->text('thumbnail_url');
            $table->string('caption', 45);
            $table->integer('width');
            $table->integer('height');
            $table->decimal('size');            
            $table->integer('listing_id')->unsigned();

            $table->foreign('listing_id')->references('id')->on('listings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_images');
    }
}