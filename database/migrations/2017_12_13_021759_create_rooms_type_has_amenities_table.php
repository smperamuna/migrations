<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTypeHasAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms_type_has_amenities', function (Blueprint $table) {
            $table->integer('room_type_id')->unsigned();
            $table->integer('amenity_id')->unsigned();

            $table->foreign('room_type_id')->references('id')->on('room_types');
            $table->foreign('amenity_id')->references('id')->on('amenities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms_type_has_amenities');
    }
}