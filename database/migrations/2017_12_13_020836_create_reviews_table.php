<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 45);
            $table->tinyInteger('rate');
            $table->text('description');
            $table->enum('states', array('new','active','disabled'));
            $table->integer('user_id')->unsigned();
            $table->dateTime('created_at');
            $table->text('host_response');
            $table->integer('listing_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('listing_id')->references('id')->on('listings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}