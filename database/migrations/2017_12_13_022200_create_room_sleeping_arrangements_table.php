<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomSleepingArrangementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_sleeping_arrangements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned();
            $table->integer('bed_type_id')->unsigned();
            $table->integer('quantity')->unsigned();

            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('bed_type_id')->references('id')->on('bed_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_sleeping_arrangements');
    }
}