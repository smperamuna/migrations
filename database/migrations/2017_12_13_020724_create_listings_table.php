<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 100);
            $table->string('name', 45);
            $table->string('address_1', 45);
            $table->string('address_2', 45);
            $table->integer('city_id')->unsigned();
            $table->string('state_province', 45);
            $table->string('postal_code', 45);
            $table->integer('country_id')->unsigned();
            $table->integer('host_id')->unsigned();
            $table->decimal('latitude', 8, 5);
            $table->decimal('longitude', 8, 5);
            $table->integer('category_id')->unsigned();
            $table->char('currency', 4);
            $table->enum('states', array('new','active','disabled'));
            $table->integer('cancellation_policy_id')->unsigned();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('host_id')->references('id')->on('hosts');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('cancellation_policy_id')->references('id')->on('cancellation_policies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listings');
    }
}