integer<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->integer('room_sleeping_arrangement_id')->unsigned();
            $table->integer('room_type_sleeping_arrangement_id')->unsigned();

            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('room_sleeping_arrangement_id')->references('id')->on('room_sleeping_arrangements');
            $table->foreign('room_type_sleeping_arrangement_id')->references('id')->on('room_type_sleeping_arrangements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_details');
    }
}